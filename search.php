<?php
/**
 * The template for displaying search results pages.
 *
 * @package moneysite
 */

get_header(); 
?>
<div class="clearfix"></div>
<main id="content">
  	<div class="container">
    	<div class="row">
      		<div class="<?php echo ( !is_active_sidebar( 'sidebar-1' ) ? 'col-md-12' :'col-md-9' ); ?>">
        		<div class="row">
        			<div class="grid">
				        <?php 
						global $i;
						if ( have_posts() ) : ?>
						<h2 class="archive-page-heading"><?php printf( esc_html__( "Search Results for: %s", 'moneysite' ), '<span>' . get_search_query() . '</span>' ); ?></h2>
						<br>
						<?php while ( have_posts() ) : the_post();  
						 get_template_part('content','');
						 endwhile; else : ?>
						<h2><?php esc_html_e('Not Found','moneysite'); ?></h2>
						<div class="">
						<p><?php esc_html_e('Sorry, Do Not match.','moneysite' ); ?>
						</p>
						<?php get_search_form(); ?>
						</div><!-- .blog_con_mn -->
						<?php endif; ?>
					</div>

					<div class="col-md-12 text-center">
			          	<?php
							//Previous / next page navigation
							the_posts_pagination( array(
							'prev_text'          => '<i class="fa fa-long-arrow-left"></i>',
							'next_text'          => '<i class="fa fa-long-arrow-right"></i>',
							'screen_reader_text' => ' ',
							) );
						?>
			        </div>
			        
        		 </div>
      		</div>
  			<aside class="col-md-3">
    			<?php get_sidebar(); ?>
  			</aside>
    	</div>
 	</div>
</main>
<?php
get_footer();
?>