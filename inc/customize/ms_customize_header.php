<?php
function moneysite_header_setting( $wp_customize ) {
/* Header Section */
	$wp_customize->add_panel( 'header_options', array(
		'priority' => 50,
		'capability' => 'edit_theme_options',
		'title' => __('Theme Header Settings', 'moneysite'),
	) );
	
	// add Header widget one Setting

    $wp_customize->add_section( 'header_widget_one' , array(
		'title' => __('Header First Widget Settings', 'moneysite'),
		'panel' => 'header_options',
		'priority'    => 600,
   	) );

   	$wp_customize->add_setting(
    	'moneysite_header_widget_one_icon', array(
        'capability'     => 'edit_theme_options',
        'sanitize_callback' => 'sanitize_text_field',
    ) );  
    $wp_customize->add_control( 
        'moneysite_header_widget_one_icon', array(
        'label' => __('Fontawesome Icon (eg: fa fa-map)','moneysite'),
        'section' => 'header_widget_one',
        'type' => 'text',
    ) );

    $wp_customize->add_setting(
        'moneysite_header_widget_one_title', array(
        'capability'     => 'edit_theme_options',
        'sanitize_callback' => 'sanitize_text_field',
    ) );  
    $wp_customize->add_control( 
        'moneysite_header_widget_one_title',array(
        'label'   => __('Title','moneysite'),
        'section' => 'header_widget_one',
        'type' => 'text',
    ) );

    $wp_customize->add_setting(
        'moneysite_header_widget_one_description', array(
        'capability' => 'edit_theme_options',
        'sanitize_callback' => 'sanitize_text_field',
    ) );  
    $wp_customize->add_control( 
        'moneysite_header_widget_one_description', array(
        'label' => __('Description','moneysite'),
        'section' => 'header_widget_one',
        'type' => 'textarea',
    ) );

    //Change first icon title color
    $wp_customize->add_setting('moneysite_first_icon_text_color', array(
    'default' => '#000',
    'sanitize_callback' => 'sanitize_hex_color',
    ) );
    
    $wp_customize->add_control(new WP_Customize_Color_Control($wp_customize,'moneysite_first_icon_text_color', array(
        'label'      => __('Title Color', 'moneysite' ),
        'section'    => 'header_widget_one',
        'settings'   => 'moneysite_first_icon_text_color',) 
    ) );

    //Change first icon description color
    $wp_customize->add_setting('moneysite_first_des_color', array(
    'default' => '#000',
    'sanitize_callback' => 'sanitize_hex_color',
    ) );
    
    $wp_customize->add_control(new WP_Customize_Color_Control($wp_customize,'moneysite_first_des_color', array(
        'label'      => __('Description Color', 'moneysite' ),
        'section'    => 'header_widget_one',
        'settings'   => 'moneysite_first_des_color',) 
    ) );

    //Change first icon backgorund color
    $wp_customize->add_setting('moneysite_first_icon_bk_color', array(
    'default' => '#6A9FDF',
    'sanitize_callback' => 'sanitize_hex_color',
    ) );
    
    $wp_customize->add_control(new WP_Customize_Color_Control($wp_customize,'moneysite_first_icon_bk_color', array(
        'label'      => __('Icon Background Color', 'moneysite' ),
        'section'    => 'header_widget_one',
        'settings'   => 'moneysite_first_icon_bk_color',) 
    ) );

    //Change first icon inner color
    $wp_customize->add_setting('moneysite_first_icon_inner_color', array(
    'default' => '#FFF',
    'sanitize_callback' => 'sanitize_hex_color',
    ) );
    
    $wp_customize->add_control(new WP_Customize_Color_Control($wp_customize,'moneysite_first_icon_inner_color', array(
        'label'      => __('Icon Color', 'moneysite' ),
        'section'    => 'header_widget_one',
        'settings'   => 'moneysite_first_icon_inner_color',) 
    ) );

    // add Header widget Two Setting
    
    $wp_customize->add_section( 'header_widget_two' , array(
		'title' => __('Header Second Widget Settings', 'moneysite'),
		'panel' => 'header_options',
		'priority'    => 620,
   	) );

   	$wp_customize->add_setting(
    	'moneysite_header_widget_two_icon', array(
		'capability'     => 'edit_theme_options',
        'sanitize_callback' => 'sanitize_text_field',
    ) );  
    $wp_customize->add_control( 
        'moneysite_header_widget_two_icon', array(
        'label' => __('Fontawesome Icon (eg: fa fa-clock-o)','moneysite'),
        'section' => 'header_widget_two',
        'type' => 'text',
    ) );

    $wp_customize->add_setting(
        'moneysite_header_widget_two_title', array(
		'capability'     => 'edit_theme_options',
        'sanitize_callback' => 'sanitize_text_field',
    ) );  
    $wp_customize->add_control( 
        'moneysite_header_widget_two_title',array(
        'label'   => __('Title','moneysite'),
        'section' => 'header_widget_two',
        'type' => 'text',
    ) );

    $wp_customize->add_setting(
        'moneysite_header_widget_two_description', array(
		'capability' => 'edit_theme_options',
        'sanitize_callback' => 'sanitize_text_field',
    ) );  
    $wp_customize->add_control( 
        'moneysite_header_widget_two_description', array(
        'label' => __('Description','moneysite'),
        'section' => 'header_widget_two',
        'type' => 'textarea',
    ) );

    //Change second icon title color
    $wp_customize->add_setting('moneysite_second_icon_text_color', array(
    'default' => '#000',
    'sanitize_callback' => 'sanitize_hex_color',
    ) );
    
    $wp_customize->add_control(new WP_Customize_Color_Control($wp_customize,'moneysite_second_icon_text_color', array(
        'label'      => __('Title Color', 'moneysite' ),
        'section'    => 'header_widget_two',
        'settings'   => 'moneysite_second_icon_text_color',) 
    ) );

    //Change second icon description color
    $wp_customize->add_setting('moneysite_second_des_color', array(
    'default' => '#000',
    'sanitize_callback' => 'sanitize_hex_color',
    ) );
    
    $wp_customize->add_control(new WP_Customize_Color_Control($wp_customize,'moneysite_second_des_color', array(
        'label'      => __('Description Color', 'moneysite' ),
        'section'    => 'header_widget_two',
        'settings'   => 'moneysite_second_des_color',) 
    ) );

    //Change second icon backgorund color
    $wp_customize->add_setting('moneysite_second_icon_bk_color', array(
    'default' => '#6A9FDF',
    'sanitize_callback' => 'sanitize_hex_color',
    ) );
    
    $wp_customize->add_control(new WP_Customize_Color_Control($wp_customize,'moneysite_second_icon_bk_color', array(
        'label'      => __('Icon Background Color', 'moneysite' ),
        'section'    => 'header_widget_two',
        'settings'   => 'moneysite_second_icon_bk_color',) 
    ) );

    //Change second icon inner color
    $wp_customize->add_setting('moneysite_second_icon_inner_color', array(
    'default' => '#FFF',
    'sanitize_callback' => 'sanitize_hex_color',
    ) );
    
    $wp_customize->add_control(new WP_Customize_Color_Control($wp_customize,'moneysite_second_icon_inner_color', array(
        'label'      => __('Icon Color', 'moneysite' ),
        'section'    => 'header_widget_two',
        'settings'   => 'moneysite_second_icon_inner_color',) 
    ) );

    // add Header widget Three Setting
    
    $wp_customize->add_section( 'header_widget_three' , array(
		'title' => __('Header Third Widget Settings', 'moneysite'),
		'panel' => 'header_options',
		'priority'    => 620,
   	) );

   	$wp_customize->add_setting(
    	'moneysite_header_widget_three_icon', array(
		'capability'     => 'edit_theme_options',
        'sanitize_callback' => 'sanitize_text_field',
        ) );  
    $wp_customize->add_control( 
        'moneysite_header_widget_three_icon', array(
        'label' => __('Fontawesome Icon (eg: fa fa-phone)','moneysite'),
        'section' => 'header_widget_three',
        'type' => 'text',
    ) );

    $wp_customize->add_setting(
        'moneysite_header_widget_three_title', array(
		'capability'     => 'edit_theme_options',
        'sanitize_callback' => 'sanitize_text_field',
    ) );  
    $wp_customize->add_control( 
        'moneysite_header_widget_three_title',array(
        'label'   => __('Title','moneysite'),
        'section' => 'header_widget_three',
        'type' => 'text',
    ) );

    $wp_customize->add_setting(
        'moneysite_header_widget_three_description', array(
        'capability' => 'edit_theme_options',
        'sanitize_callback' => 'sanitize_text_field',
    ) );  
    $wp_customize->add_control( 
        'moneysite_header_widget_three_description', array(
        'label' => __('Description','moneysite'),
        'section' => 'header_widget_three',
        'type' => 'textarea',
    ) );

    //Change second icon title color
    $wp_customize->add_setting('moneysite_third_icon_text_color', array(
    'default' => '#000',
    'sanitize_callback' => 'sanitize_hex_color',
    ) );
    
    $wp_customize->add_control(new WP_Customize_Color_Control($wp_customize,'moneysite_third_icon_text_color', array(
        'label'      => __('Title Color', 'moneysite' ),
        'section'    => 'header_widget_three',
        'settings'   => 'moneysite_third_icon_text_color',) 
    ) );

    //Change second icon description color
    $wp_customize->add_setting('moneysite_third_des_color', array(
    'default' => '#000',
    'sanitize_callback' => 'sanitize_hex_color',
    ) );
    
    $wp_customize->add_control(new WP_Customize_Color_Control($wp_customize,'moneysite_third_des_color', array(
        'label'      => __('Description Color', 'moneysite' ),
        'section'    => 'header_widget_three',
        'settings'   => 'moneysite_third_des_color',) 
    ) );
 
    //Change Third icon backgorund color
    $wp_customize->add_setting('moneysite_third_icon_bk_color', array(
    'default' => '#6A9FDF',
    'sanitize_callback' => 'sanitize_hex_color',
    ) );
    
    $wp_customize->add_control(new WP_Customize_Color_Control($wp_customize,'moneysite_third_icon_bk_color', array(
        'label'      => __('Icon Background Color', 'moneysite' ),
        'section'    => 'header_widget_three',
        'settings'   => 'moneysite_third_icon_bk_color',) 
    ) );

    //Change Third icon inner color
    $wp_customize->add_setting('moneysite_third_icon_inner_color', array(
    'default' => '#FFF',
    'sanitize_callback' => 'sanitize_hex_color',
    ) );

    $wp_customize->add_control(new WP_Customize_Color_Control($wp_customize,'moneysite_third_icon_inner_color', array(
        'label'      => __('Icon Color', 'moneysite' ),
        'section'    => 'header_widget_three',
        'settings'   => 'moneysite_third_icon_inner_color',) 
    ) );

    
    /* Header Fourth Quote */
    
    $wp_customize->add_section( 'header_widget_four' , array(
        'title' => __('Header Button Settings', 'moneysite'),
        'panel' => 'header_options',
        'priority'    => 620,
    ) );

    $wp_customize->add_setting(
        'moneysite_header_widget_four_label', array(
		'capability'     => 'edit_theme_options',
        'sanitize_callback' => 'sanitize_text_field',
    ) );  
    $wp_customize->add_control( 
        'moneysite_header_widget_four_label', array(
        'label' => __('Label For Button','moneysite'),
        'section' => 'header_widget_four',
        'type' => 'text',
    ) );

    $wp_customize->add_setting(
        'moneysite_header_widget_four_link', array(
		'capability'     => 'edit_theme_options',
        'sanitize_callback' => 'esc_url_raw',
    ) );  
    $wp_customize->add_control( 
        'moneysite_header_widget_four_link',array(
        'label'   => __('Link','moneysite'),
        'section' => 'header_widget_four',
        'type' => 'text',
    ) );

    $wp_customize->add_setting(
        'moneysite_header_widget_four_target', array(
		'capability' => 'edit_theme_options',
        'sanitize_callback' => 'moneysite_header_sanitize_checkbox',
    ) );  
    $wp_customize->add_control( 
        'moneysite_header_widget_four_target', array(
        'label' => __('Open Link New window','moneysite'),
        'section' => 'header_widget_four',
        'type' => 'checkbox',
    ) );
	
    //Change Quote Button backgorund color
    $wp_customize->add_setting('moneysite_quote_button_bk_color', array(
    'default' => '#6A9FDF',
    'sanitize_callback' => 'sanitize_hex_color',
    ) );
    
    $wp_customize->add_control(new WP_Customize_Color_Control($wp_customize,'moneysite_quote_button_bk_color', array(
        'label'      => __('Header Button Background Color', 'moneysite' ),
        'section'    => 'header_widget_four',
        'settings'   => 'moneysite_quote_button_bk_color',) 
    ) );

    //Change Quote Button text color
    $wp_customize->add_setting('moneysite_quote_button_text_color', array(
    'default' => '#FFF',
    'sanitize_callback' => 'sanitize_hex_color',
    ) );

    $wp_customize->add_control(new WP_Customize_Color_Control($wp_customize,'moneysite_quote_button_text_color', array(
        'label'      => __('Header Button Text Color', 'moneysite' ),
        'section'    => 'header_widget_four',
        'settings'   => 'moneysite_quote_button_text_color',) 
    ) );

    // Change Quote Button Hover color
    $wp_customize->add_setting('moneysite_quote_button_hover_color', array(
    'default' => '#FFF',
    'sanitize_callback' => 'sanitize_hex_color',
    ) );

    $wp_customize->add_control(new WP_Customize_Color_Control($wp_customize,'moneysite_quote_button_hover_color', array(
        'label'      => __('Header Button Hover Color', 'moneysite' ),
        'section'    => 'header_widget_four',
        'settings'   => 'moneysite_quote_button_hover_color',) 
    ) );

    /********* Grid/List View *************/

    $wp_customize->add_section( 'grid_list_view' , array(
        'title' => __('Grid/List Post View', 'moneysite'),
        'priority'    => 60,
    ) );

    //add setting to your section
    $wp_customize->add_setting( 
        'grid_list_view_proprety', 
        array(
            'default' => 'grid',
            'sanitize_callback' => 'moneysite_sanitize_radio'
        )
    );

    $wp_customize->add_control( 
            'grid_list_view_proprety', 
            array(
                'label' => esc_html__( 'Your Setting with Radio Box', 'moneysite' ),
                'section' => 'grid_list_view',
                'type' => 'radio',
                'choices' => array(
                    'grid' => esc_html__('Grid View','moneysite'),
                    'list' => esc_html__('List view','moneysite'),               
                )
            )
        );

    //radio box sanitization function
    function moneysite_sanitize_radio( $input, $setting ){
     
        //input must be a slug: lowercase alphanumeric characters, dashes and underscores are allowed only
        $input = sanitize_key($input);

        //get the list of possible radio box options 
        $choices = $setting->manager->get_control( $setting->id )->choices;
                         
        //return input if valid or return default option
        return ( array_key_exists( $input, $choices ) ? $input : $setting->default );                
         
    }

	function moneysite_header_sanitize_checkbox( $input ) {
	// Boolean check 
	return ( ( isset( $input ) && true == $input ) ? true : false );
	}
	
	}
	add_action( 'customize_register', 'moneysite_header_setting' );
	?>