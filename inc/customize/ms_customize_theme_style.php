<?php 
// Adding customizer home page setting
function moneysite_style_customizer( $wp_customize ){
	
	/* Panel For Theme Colors */

	$wp_customize->add_panel( 'theme_colors', array(
		'priority' => 52,
		'capability' => 'edit_theme_options',
		'title' => __('Theme Colors', 'moneysite'),
	) );

	/* Theme Header Style settings */
	$wp_customize->add_section( 'moneysite_theme_skin_color', array(
		'title' => __('Set Header Colors', 'moneysite'),
		'panel' => 'header_options', /* (header_options) panel exists in ms_customize_header.php */
   	) );
	
	$wp_customize->add_setting('moneysite_hedaer_skin_enable', array(
        'default' => 'false',
		'sanitize_callback' => 'moneysite_style_sanitize_checkbox',
    ) );
	$wp_customize->add_control('moneysite_hedaer_skin_enable', array(
        'label' => __('Enable/Disable Header Custom Color Feature','moneysite'),
        'section' => 'moneysite_theme_skin_color',
        'type' => 'radio',
		'choices' => array(true => 'On', false => 'Off',)
    ) );
	
	//header background color
	$wp_customize->add_setting('moneysite_header_background', array(
	'default' => '#fff',
	'sanitize_callback' => 'sanitize_hex_color',
    ) );
	
	$wp_customize->add_control(new WP_Customize_Color_Control($wp_customize,'moneysite_header_background', array(
		'label'      => __('Header Background Color', 'moneysite' ),
		'section'    => 'moneysite_theme_skin_color',
		'settings'   => 'moneysite_header_background',) 
	) );

	//Site Title Text color
	$wp_customize->add_setting('moneysite_site_title_color', array(
		'default' => '#303f9f',
		'sanitize_callback' => 'sanitize_hex_color',
    ) );
	
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize,'moneysite_site_title_color', array(
		'label'      => __('Site Title Text Color', 'moneysite' ),
		'section'    => 'moneysite_theme_skin_color',
		'settings'   => 'moneysite_site_title_color',) 
	) );

	
	//Menu Bar Background color
	$wp_customize->add_setting('moneysite_menu_bar', array(
		'default' => '#fff',
		'sanitize_callback' => 'sanitize_hex_color',
    ) );
	
	$wp_customize->add_control(new WP_Customize_Color_Control($wp_customize,'moneysite_menu_bar', array(
		'label' => __('Menu Bar Background Color', 'moneysite' ),
		'section' => 'moneysite_theme_skin_color',
		'settings' => 'moneysite_menu_bar',) 
	) );
	
	//Menu Background color
	$wp_customize->add_setting('moneysite_menu_background', array(
		'default' => '#fff',
		'sanitize_callback' => 'sanitize_hex_color',
    ) );
	
	$wp_customize->add_control(new WP_Customize_Color_Control($wp_customize,'moneysite_menu_background', array(
		'label' => __('Menu Background Color', 'moneysite' ),
		'section' => 'moneysite_theme_skin_color',
		'settings' => 'moneysite_menu_background',) 
	) );

	//Menu Background Hover color
	$wp_customize->add_setting('moneysite_menu_background_hover', array(
		'default' => '#f5f5f5',
		'sanitize_callback' => 'sanitize_hex_color',
    ) );
	
	$wp_customize->add_control(new WP_Customize_Color_Control($wp_customize,'moneysite_menu_background_hover', array(
		'label'      => __('Menu Background Hover/Active Color', 'moneysite' ),
		'section'    => 'moneysite_theme_skin_color',
		'settings'   => 'moneysite_menu_background_hover',) 
	) );

	//Menu Border color Bottom color
	$wp_customize->add_setting('moneysite_menu_border_color', array(
		'default' => '#fff',
		'sanitize_callback' => 'sanitize_hex_color',
    ) );
	
	$wp_customize->add_control(new WP_Customize_Color_Control($wp_customize,'moneysite_menu_border_color', array(
		'label'      => __('Menu Border Bottom color', 'moneysite' ),
		'section'    => 'moneysite_theme_skin_color',
		'settings'   => 'moneysite_menu_border_color',) 
	) );

	//Menu Border color Bottom Hover/Active color
	$wp_customize->add_setting('moneysite_menu_border_active_color', array(
		'default' => '#303f9f',
		'sanitize_callback' => 'sanitize_hex_color',
    ) );
	
	$wp_customize->add_control(new WP_Customize_Color_Control($wp_customize,'moneysite_menu_border_active_color', array(
		'label'      => __('Menu Border Bottom Hover/Active color', 'moneysite' ),
		'section'    => 'moneysite_theme_skin_color',
		'settings'   => 'moneysite_menu_border_active_color',) 
	) );

	//Menu text color & Menu active color
	$wp_customize->add_setting('moneysite_menu_color', array(
		'default' => '#333',
		'sanitize_callback' => 'sanitize_hex_color',
    ) );
	
	$wp_customize->add_control(new WP_Customize_Color_Control($wp_customize,'moneysite_menu_color', array(
		'label'      => __('Menu Text Color', 'moneysite' ),
		'section'    => 'moneysite_theme_skin_color',
		'settings'   => 'moneysite_menu_color',) 
	) );
	
	//Menu active color-radio-
	$wp_customize->add_setting('moneysite_menu_active_color', array(
		'default' => '#303f9f',
		'sanitize_callback' => 'sanitize_hex_color',
    ) );
	
	$wp_customize->add_control(new WP_Customize_Color_Control($wp_customize,'moneysite_menu_active_color', array(
		'label' => __('Menu Text Hover/Active Color', 'moneysite' ),
		'section' => 'moneysite_theme_skin_color',
		'settings' => 'moneysite_menu_active_color',) 
	) );

	//Sub Menu Background Color
	$wp_customize->add_setting('moneysite_menu_submenu_background', array(
		'default' => '#fff',
		'sanitize_callback' => 'sanitize_hex_color',
    ) );
	
	$wp_customize->add_control(new WP_Customize_Color_Control( $wp_customize,'moneysite_menu_submenu_background', array(
		'label' => __('Sub Menu Background Color', 'moneysite' ),
		'section' => 'moneysite_theme_skin_color',
		'settings' => 'moneysite_menu_submenu_background',) 
	) );
	
	
	//Sub Menu Hover Color
	$wp_customize->add_setting('moneysite_menu_submenu_background_hover', array(
		'default' => '#303f9f',
		'sanitize_callback' => 'sanitize_hex_color',
    ) );
	
	$wp_customize->add_control(new WP_Customize_Color_Control( $wp_customize,'moneysite_menu_submenu_background_hover', array(
		'label'      => __('Sub Menu Background Hover color', 'moneysite' ),
		'section'    => 'moneysite_theme_skin_color',
		'settings'   => 'moneysite_menu_submenu_background_hover',) 
	) );
	
	//Sub Menu text Color
	$wp_customize->add_setting('moneysite_menu_submenu_color', array(
		'default' => '#333',
		'sanitize_callback' => 'sanitize_hex_color',
    ) );
	
	$wp_customize->add_control(new WP_Customize_Color_Control($wp_customize,'moneysite_menu_submenu_color', array(
		'label'      => __('Sub Menu Text Color', 'moneysite' ),
		'section'    => 'moneysite_theme_skin_color',
		'settings'   => 'moneysite_menu_submenu_color',) 
	) );

	/* Theme Footer Style settings */
	$wp_customize->add_section( 'moneysite_footer_color' , array(
		'title' => __('Set Footer Colors', 'moneysite'),
		'panel' => 'moneysite_copyright', /* (moneysite_copyright) Panel exists in ms_customize_copyright.php */
   	) );
	
	//Footer Enable Color
	$wp_customize->add_setting('moneysite_footer_color_enable', array(
        'default' => 'true',
		'sanitize_callback' => 'moneysite_style_sanitize_checkbox',
		
    ) );
	$wp_customize->add_control('moneysite_footer_color_enable', array(
        'label' => __('Enable/Disable Footer Custom Color Feature','moneysite'),
        'section' => 'moneysite_footer_color',
        'type' => 'radio',
		'choices' => array(true => 'On', false => 'Off',)
    ) );
	
	
	//Footer background
	$wp_customize->add_setting('moneysite_footer_background', array(
		'default' => '#349BD6',
		'sanitize_callback' => 'sanitize_hex_color',
    ) );
	
	$wp_customize->add_control(new WP_Customize_Color_Control($wp_customize,'moneysite_footer_background', array(
		'label' => __('Footer Background Color ', 'moneysite' ),
		'section' => 'moneysite_footer_color',
		'settings' => 'moneysite_footer_background',) 
	) );
	
	//Footer Widget Heading color
	$wp_customize->add_setting('moneysite_footer_head_color', array(
		'default' => '#fff',
		'sanitize_callback' => 'sanitize_hex_color',
    ) );
	
	$wp_customize->add_control(new WP_Customize_Color_Control($wp_customize,'moneysite_footer_head_color', array(
		'label' => __('Footer Widget Heading Color ', 'moneysite' ),
		'section' => 'moneysite_footer_color',
		'settings' => 'moneysite_footer_head_color',) 
	) );

	//Footer color
	$wp_customize->add_setting('moneysite_footer_text_color', array(
	'default' => '#FFF',
	'sanitize_callback' => 'sanitize_hex_color',
    ) );
	
	$wp_customize->add_control( new WP_Customize_Color_Control($wp_customize,'moneysite_footer_text_color', array(
		'label' => __('Footer Text Color ', 'moneysite' ),
		'section' => 'moneysite_footer_color',
		'settings' => 'moneysite_footer_text_color',) 
	) );

	//Footer Copyright background
	$wp_customize->add_setting('moneysite_footer_copy_background', array(
		'default' => '#349BD6',
		'sanitize_callback' => 'sanitize_hex_color',
    ) );
	
	$wp_customize->add_control(new WP_Customize_Color_Control( $wp_customize,'moneysite_footer_copy_background', array(
		'label' => __('Footer Copyright Background Color ', 'moneysite' ),
		'section' => 'moneysite_footer_color',
		'settings' => 'moneysite_footer_copy_background',) 
	) );

	//Footer Copyright Color
	$wp_customize->add_setting('moneysite_footer_copy_color', array(
		'default' => '#FFF',
		'sanitize_callback' => 'sanitize_hex_color',
    ) );
	
	$wp_customize->add_control(new WP_Customize_Color_Control( $wp_customize,'moneysite_footer_copy_color', array(
		'label' => __('Footer Copyright Text Color ', 'moneysite' ),
		'section' => 'moneysite_footer_color',
		'settings' => 'moneysite_footer_copy_color',) 
	) );
	

	/* Theme Footer Style settings end */

	// Section For Theme Button Color

    $wp_customize->add_section( 'theme_button_colors' , array(
		'title' => __('Theme Buttons Color', 'moneysite'),
		'panel' => 'theme_colors',
		'priority'    => 600,
   	) );

    //Change Buttons Color
	$wp_customize->add_setting('moneysite_button_bk_color', array(
	'default' => '#6A9FDF',
	'sanitize_callback' => 'sanitize_hex_color',
    ) );
	
	$wp_customize->add_control(new WP_Customize_Color_Control($wp_customize,'moneysite_button_bk_color', array(
		'label'      => __('Change Button Colors', 'moneysite' ),
		'section'    => 'theme_button_colors',
		'settings'   => 'moneysite_button_bk_color',) 
	) );

	//Change Button Text Color
	$wp_customize->add_setting('moneysite_button_text_color', array(
	'default' => '#FFF',
	'sanitize_callback' => 'sanitize_hex_color',
    ) );
	
	$wp_customize->add_control(new WP_Customize_Color_Control($wp_customize,'moneysite_button_text_color', array(
		'label'      => __('Change Button Text Color', 'moneysite' ),
		'section'    => 'theme_button_colors',
		'settings'   => 'moneysite_button_text_color',) 
	) );

	//Change Button Hover Color
	$wp_customize->add_setting('moneysite_button_hover_color', array(
	'default' => '#333',
	'sanitize_callback' => 'sanitize_hex_color',
    ) );
	
	$wp_customize->add_control(new WP_Customize_Color_Control($wp_customize,'moneysite_button_hover_color', array(
		'label'      => __('Change Button Hover Color', 'moneysite' ),
		'section'    => 'theme_button_colors',
		'settings'   => 'moneysite_button_hover_color',) 
	) );

	// Section For Page Navigation Colors

    $wp_customize->add_section( 'page_navigation_colors' , array(
		'title' => __('Page Navigation Bar Color', 'moneysite'),
		'panel' => 'theme_colors',
		'priority'    => 600,
   	) );

    /* Change page Navigation Colors */

	$wp_customize->add_setting('page_nav_bk_color', array(
		'default' => '#349BD6',
		'sanitize_callback' => 'sanitize_hex_color',
    ) );
	
	$wp_customize->add_control(new WP_Customize_Color_Control( $wp_customize,'page_nav_bk_color', array(
		'label' => __('Page Navigation Background Color ', 'moneysite' ),
		'section' => 'page_navigation_colors',
		'settings' => 'page_nav_bk_color',) 
	) );

	$wp_customize->add_setting('page_nav_text_color', array(
		'default' => '#fff',
		'sanitize_callback' => 'sanitize_hex_color',
    ) );
	
	$wp_customize->add_control(new WP_Customize_Color_Control( $wp_customize,'page_nav_text_color', array(
		'label' => __('Page Navigation Text Color ', 'moneysite' ),
		'section' => 'page_navigation_colors',
		'settings' => 'page_nav_text_color',) 
	) );

	$wp_customize->add_setting('page_nav_hover_color', array(
		'default' => '#4169e1',
		'sanitize_callback' => 'sanitize_hex_color',
    ) );
	
	$wp_customize->add_control(new WP_Customize_Color_Control( $wp_customize,'page_nav_hover_color', array(
		'label' => __('Page Navigation Hover Color ', 'moneysite' ),
		'section' => 'page_navigation_colors',
		'settings' => 'page_nav_hover_color',) 
	) );

	$wp_customize->add_setting('page_nav_active_color', array(
		'default' => '#4169e1',
		'sanitize_callback' => 'sanitize_hex_color',
    ) );
	
	$wp_customize->add_control(new WP_Customize_Color_Control( $wp_customize,'page_nav_active_color', array(
		'label' => __('Page Navigation Active Button Color ', 'moneysite' ),
		'section' => 'page_navigation_colors',
		'settings' => 'page_nav_active_color',) 
	) );

	function moneysite_style_sanitize_checkbox( $input ) {
	    $valid = array(
	        true => 'On',
    		false => 'Off',
	    );
	 
	    if ( array_key_exists( $input, $valid ) ) {
	        
	        if($input == '1'){
	        	return true;
	        }
	        elseif($input == '0'){
	        	return false;
	        }

	    } else {
	        return false;
	    }
	}
	
}
add_action( 'customize_register', 'moneysite_style_customizer' );