<?php
function moneysite_scripts() {
	
	wp_enqueue_style('bootstrap_style_min', get_template_directory_uri() . '/css/bootstrap.min.css');

	wp_enqueue_style( 'moneysite-style', get_stylesheet_uri() );

	wp_enqueue_style('moneysite_color', get_template_directory_uri() . '/css/colors/moneysite-default.css');
	
	wp_enqueue_style('font-awesome-min',get_template_directory_uri().'/css/font-awesome.min.css');

	wp_enqueue_style('animate_min',get_template_directory_uri().'/css/animate.min.css');

	/* Js script */

    wp_enqueue_script( 'moneysite-navigation', get_template_directory_uri() . '/js/moneysite-navigation.js', array('jquery'));

	wp_enqueue_script('bootstrap-js', get_template_directory_uri() . '/js/bootstrap.js', array('jquery'));

    wp_enqueue_script('smartmenus', get_template_directory_uri() . '/js/jquery.smartmenus.min.js' , array('jquery'));

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action('wp_enqueue_scripts', 'moneysite_scripts');

//Header Custom Color
function moneysite_header_color() {
    $moneysite_hedaer_skin_enable = get_theme_mod('moneysite_hedaer_skin_enable','false');
    if( $moneysite_hedaer_skin_enable == true) {
    moneysite_custom_header_color();
    }
}
add_action('wp_footer','moneysite_header_color');

//Button and Header Custom Color
function moneysite_button_color() {
    	moneysite_custom_button_color();
}
add_action('wp_footer','moneysite_button_color');

//Footer Custom Color
function moneysite_footer_color() {
    $moneysite_footer_color_enable = get_theme_mod('moneysite_footer_color_enable','true');
    if( $moneysite_footer_color_enable == true) {
        moneysite_custom_footer_color();
    }
}
add_action('wp_footer','moneysite_footer_color');

?>