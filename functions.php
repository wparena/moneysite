<?php
/**
 * moneysite functions and definitions.
 *
 *
 * @package moneysite
 */


	$moneysite_theme_path = get_template_directory() . '/inc/';

	require( $moneysite_theme_path . '/moneysite-custom-navwalker.php' );
	require( $moneysite_theme_path . '/font/font.php');

	/*-----------------------------------------------------------------------------------*/
	/*	Enqueue scripts and styles.
	/*-----------------------------------------------------------------------------------*/
	require( $moneysite_theme_path .'/enqueue.php');
	/* ----------------------------------------------------------------------------------- */
	/* Customizer */
	/* ----------------------------------------------------------------------------------- */

	require( $moneysite_theme_path . '/customize/ms_customize_header.php');
	require( $moneysite_theme_path . '/customize/ms_customize_theme_style.php');
	require( $moneysite_theme_path . '/customize/ms_customize_copyright.php');
	
	require( $moneysite_theme_path . '/customize/customize_control/class-moneysite-customize-alpha-color-control.php');
	

if ( ! function_exists( 'moneysite_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function moneysite_setup() {
	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on moneysite, use a find and replace
	 * to change 'moneysite' to the name of your theme in all the template files.
	 */
	load_theme_textdomain( 'moneysite', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
	 */
	add_theme_support( 'post-thumbnails' );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'primary' => __('Primary menu','moneysite' )
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );
	
	//Custom Logo
	add_theme_support( 'custom-logo');

	function moneysite_the_custom_logo() {
		the_custom_logo();
	}

	add_filter('get_custom_logo','moneysite_logo_class');


	function moneysite_logo_class($html)
	{
	$html = str_replace('custom-logo-link', 'navbar-brand', $html);
	return $html;
	}

	// Set up the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( 'moneysite_custom_background_args', array(
		'default-color' => 'eeeeee',
		'default-image' => '',
	) ) );

}
endif;
add_action( 'after_setup_theme', 'moneysite_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function moneysite_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'moneysite_content_width', 640 );
}
add_action( 'after_setup_theme', 'moneysite_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function moneysite_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'moneysite' ),
		'id'            => 'sidebar-1',
		'description'   => '',
		'before_widget' => '<div id="%1$s" class="ms-widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h6>',
		'after_title'   => '</h6>',
	) );

	register_sidebar( array(
		'name'          => esc_html__( 'Footer Widget Area', 'moneysite' ),
		'id'            => 'footer_widget_area',
		'description'   => '',
		'before_widget' => '<div id="%1$s" class="col-md-6 col-sm-6 rotateInDownLeft animated ms-widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h6>',
		'after_title'   => '</h6>',
	) );
}
add_action( 'widgets_init', 'moneysite_widgets_init' );


function moneysite_enqueue_customizer_controls_styles() {
  	wp_register_style( 'moneysite-customizer-controls', get_template_directory_uri() . '/css/moneysite-customizer-controls.css', NULL, NULL, 'all' );
  	wp_enqueue_style( 'moneysite-customizer-controls' );
}

add_action( 'customize_controls_print_styles', 'moneysite_enqueue_customizer_controls_styles' );

/* custom-color file. */
require( get_template_directory() . '/css/colors/moneysite-custom-color.php');

//Read more Button on slider & Post
function moneysite_read_more() {
	if ( ! is_admin() ){
		global $post;
		
		$readbtnurl = '<br><a class="btn btn-tislider-two" href="' . esc_url(get_permalink()) . '">'.__('Read More','moneysite').'</a>';
		
	    return $readbtnurl;
	}
}
add_filter( 'the_content_more_link', 'moneysite_read_more' );

/**

* Add a pingback url auto-discovery header for singularly identifiable articles.

*/

function moneysite_pingback_header() {

    if ( is_singular() && pings_open() ) {

        printf( '<link rel="pingback" href="%s">' . "\n", esc_url(get_bloginfo( 'pingback_url' )) );

    }

}

add_action( 'wp_head', 'moneysite_pingback_header' );

