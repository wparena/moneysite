<?php
/**
 * The template for displaying 404 pages (not found).
 *
 * @package moneysite
 */

get_header(); ?>

<main id="content">
  <div class="container">
    <div class="row">
      <div class="col-md-12 text-center ms-section">
        <div id="primary" class="content-area">
          <main id="main" class="site-main" role="main">
            <div class="ms-error-404">
              <h1><?php esc_html_e( '4','moneysite' ); ?><i class="fa fa-times-circle-o"></i><?php esc_html_e( '4','moneysite' ); ?></h1>
              <h4>
                <?php esc_html_e( 'Oops! That page can&rsquo;t be found.', 'moneysite' ); ?>
              </h4>
              <p>
                <?php esc_html_e( 'It looks like nothing was found at this location. Maybe try one of the links below or a search?', 'moneysite' ); ?>
              </p>
              <a href="<?php echo esc_url(home_url());?>" onClick="history.back();" class="btn btn-theme"><?php esc_html_e( 'Go Back','moneysite' ); ?></a> 
            </div>
            <section class="error-404 not-found">
              <header class="page-header">
                <h1 class="page-title">Go for a search</h1>
              </header>
              <!-- .page-header -->
              
              <div class="page-content">
                <div class="col-md-4 col-md-offset-4">
                  <div class="ms-sidebar">
                    <?php
						          get_search_form();
					          ?>
                  </div>
                </div>
              </div>
              <!-- .page-content --> 
            </section>
            <!-- .error-404 --> 
          </main>
          <!-- #main --> 
        </div>
        <!-- #primary --> 
      </div>
    </div>
  </div>
</main>
<?php
get_footer();
?>