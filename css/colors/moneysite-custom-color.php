<?php

/*----Custom Header Color----*/ 
function moneysite_custom_header_color() {
$moneysite_top_background = get_theme_mod('moneysite_top_background');
$moneysite_top_color = get_theme_mod('moneysite_top_color');
$moneysite_site_title_color = get_theme_mod('moneysite_site_title_color');
$moneysite_header_background = get_theme_mod('moneysite_header_background');
$moneysite_menu_background = get_theme_mod('moneysite_menu_background');
$moneysite_menu_bar = get_theme_mod('moneysite_menu_bar');
$moneysite_menu_background_hover = get_theme_mod('moneysite_menu_background_hover');
$moneysite_menu_border_color = get_theme_mod('moneysite_menu_border_color');
$moneysite_menu_border_active_color = get_theme_mod('moneysite_menu_border_active_color');
$moneysite_menu_color = get_theme_mod('moneysite_menu_color');
$moneysite_menu_active_color = get_theme_mod('moneysite_menu_active_color');
$moneysite_menu_submenu_background = get_theme_mod('moneysite_menu_submenu_background');
$moneysite_menu_submenu_background_hover = get_theme_mod('moneysite_menu_submenu_background_hover');
$moneysite_menu_submenu_color = get_theme_mod('moneysite_menu_submenu_color');
?>
<style type="text/css">
.ms-head-detail{
	background: <?php echo esc_attr( $moneysite_top_background);?>;
}

.ms-head-detail .info-left li a , .ms-head-detail li a i, .ms-head-detail .info-right li a {
	color: <?php echo esc_attr( $moneysite_top_color);?>;
}
/*=== navbar menu Background hover color ===*/ 
.navbar-wp .navbar-nav > li > a:hover, .navbar-wp .navbar-nav > li > a:focus, .navbar-wp .navbar-nav > li > a.active {
	background:<?php echo esc_attr( $moneysite_menu_background_hover);?>;
}
/*=== navbar background color ===*/ 
.navbar-wp .navbar-nav > li > a {
	background:<?php echo esc_attr( $moneysite_menu_background);?>;
}
/*=== navbar text color ===*/ 
.navbar-wp .navbar-nav > li > a {
	color: <?php echo esc_attr( $moneysite_menu_color);?>;
}
/*=== navbar text color ===*/ 
.navbar-wp .navbar-nav > li > a:hover, .navbar-wp .navbar-nav > li > a:focus, .navbar-wp .navbar-nav > li > a.active {
	color:<?php echo esc_attr( $moneysite_menu_active_color);?>;
}
/*=== navbar hover border-color ===*/
.navbar-wp .navbar-nav > li > a {
	border-color: <?php echo esc_attr($moneysite_menu_border_color);?>;
}
/*=== navbar hover active ===*/
.navbar-wp .navbar-nav > .active > a, .navbar-wp .navbar-nav > .active > a:hover, .navbar-wp .navbar-nav > .active > a:focus, .navbar-wp .navbar-nav > li > a:hover, .navbar-wp .navbar-nav > li > a:focus, .navbar-wp .navbar-nav > .open > a, .navbar-wp .navbar-nav > .open > a:hover, .navbar-wp .navbar-nav > .open > a:focus, .navbar-wp .navbar-nav > li > a:hover, .navbar-wp .navbar-nav > li >a:focus {
	background-color:<?php echo esc_attr( $moneysite_menu_background_hover);?>;
}
/*=== navbar Menu Border Bottom Hover color ===*/ 
.navbar-wp .navbar-nav > li > ul::before {
	border-color: rgba(0, 0, 0, 0) rgba(0, 0, 0, 0) <?php echo esc_attr( $moneysite_menu_border_active_color);?>;
}
.navbar-wp .navbar-nav > li > a:hover, .navbar-wp .navbar-nav > li > a:focus,  .navbar-wp .navbar-nav > .active > a, .navbar-wp .navbar-nav > .active > a:hover, .navbar-wp .navbar-nav > .active > a:focus, .navbar-wp .navbar-nav > .open > a, .navbar-wp .navbar-nav > .open > a:hover, .navbar-wp .navbar-nav > .open > a:focus, .navbar-wp .navbar-toggle:hover, .navbar-wp .navbar-toggle:focus {
	border-color: <?php echo esc_attr( $moneysite_menu_border_active_color);?>;
}
.navbar-wp .navbar-nav > .dropdown > a .caret, .navbar-wp .navbar-nav > li > a:hover, .navbar-wp .navbar-nav > li > a:focus, .navbar-wp .navbar-nav > .active > a, .navbar-wp .navbar-nav > .active > a:hover, .navbar-wp .navbar-nav > .active > a:focus {
	border-bottom-color:<?php echo esc_attr( $moneysite_menu_border_active_color);?>;
}

/*=== navbar dropdown text color ===*/ 
.navbar-toggle, .navbar-wp .dropdown-menu > li > a, .navbar-wp .navbar-nav > li > a.dropdown-form-toggle {
	color: <?php echo esc_attr( $moneysite_menu_submenu_color);?>;
}
/*=== navbar dropdown background Hover color ===*/ 
.ms-search-box .ms-search .btn, .navbar-wp .dropdown-menu > a, .navbar-wp .dropdown-menu > li > a, .navbar-wp .navbar-toggle {
	background: <?php echo esc_attr( $moneysite_menu_submenu_background);?>;
}
/*=== navbar dropdown background Hover color ===*/ 
.navbar-wp .dropdown-menu > .active > a, .navbar-wp .dropdown-menu > .active > a:hover, .navbar-wp .dropdown-menu > .active > a:focus, .navbar-wp .dropdown-menu > li > a:hover, .navbar-wp .navbar-toggle:hover, .navbar-wp .navbar-toggle:focus, .ms-search-box .ms-search .btn {
	background: <?php echo esc_attr( $moneysite_menu_submenu_background_hover);?>;
}

/*=== navbar Title color ===*/ 
.navbar-header .navbar-brand {
	color: <?php echo esc_attr( $moneysite_site_title_color);?>;
}
/*=== navbar background color ===*/ 
.ms-main-nav {
	background: <?php echo esc_attr( $moneysite_header_background);?>;
}
.navbar-wp {
	background: <?php echo esc_attr( $moneysite_menu_bar);?>;
}
</style>
<?php
}

/*----Custom Footer Color----*/ 
function moneysite_custom_footer_color() {
$moneysite_footer_background = get_theme_mod('moneysite_footer_background','#349BD6');
$moneysite_footer_head_color = get_theme_mod('moneysite_footer_head_color');
$moneysite_footer_text_color = get_theme_mod('moneysite_footer_text_color','#fff');
$moneysite_footer_copy_background = get_theme_mod('moneysite_footer_copy_background','#349BD6');
$moneysite_footer_copy_color = get_theme_mod('moneysite_footer_copy_color');
?>
<style type="text/css">
/*==================== footer background ====================*/
footer {
	background: <?php echo esc_attr( $moneysite_footer_background);?>;
}
footer .ms-footer-copyright {
	background: <?php echo esc_attr( $moneysite_footer_copy_background);?>;
}
footer .ms-footer-copyright p, footer .ms-footer-copyright a {
	color: <?php echo esc_attr( $moneysite_footer_copy_color);?>;
}
footer .ms-footer-copyright a:hover, footer .ms-footer-copyright a:focus {
	color: #fff;
}
footer .ms-footer-widget-area {
	border-top-color: rgba(225,225,225,0.2);
}
/*==================== footer color ====================*/
/*=== footer heading color ===*/
footer .ms-widget h6 {
	color: <?php echo esc_attr( $moneysite_footer_head_color);?>;
}

footer .ms-widget .calendar_wrap table thead th, label, footer p, footer .ms-blog-post span, footer .ms-widget .textwidget, footer a, footer .ms-widget .list-unstyled li a, footer .ms-widget .ms-twitter-feed li, footer .ms-widget .ms-widget-address li, footer .ms-widget .ms-social li span.icon-soci, footer .ms-widget .ms-opening-hours li, footer .ms-widget .ms-widget-tags a ,footer .ms-widget .tagcloud a, footer .ms-widget-quote .form-control, footer .ms-widget-tollfree li a, footer .ms-widget-tollfree li i, footer .ms-widget-payment a, footer .ms-calendar a:hover, footer .ms-calendar thead th, footer .wpcf7-form p, footer .ms-widget .wpcf7-form .wpcf7-form-control {
	color: <?php echo esc_attr( $moneysite_footer_text_color);?>;
}

footer .ms-widget .form-control, footer .ms-widget ul li, footer .ms-widget .list-unstyled li, footer .ms-widget .ms-social li span.icon-soci, footer .calendar_wrap caption, footer .ms-widget .ms-widget-tags a, footer .ms-widget .tagcloud a, footer .calendar_wrap table thead th  {
	border-color: <?php echo esc_attr( $moneysite_footer_text_color);?>;
}
/*==================== footer hover color ====================*/
footer a:hover, footer a:focus, footer .ms-widget .ms-subscribe:hover, footer .ms-widget .ms-subscribe:focus, footer .ms-widget .ms-search-widget .btn:hover, footer .ms-widget .ms-search-widget .btn:focus, footer .ms-widget .list-unstyled li a:hover, footer .ms-widget .ms-opening-hours li:hover, footer .ms-widget .ms-widget-address li span.icon-addr, footer .ms-widget .ms-social li span.icon-soci:hover i, footer .ms-widget .ms-social li span.icon-soci:hover, footer .ms-widget .ms-widget-tags a:hover ,footer .ms-widget .tagcloud a:hover, footer .ms-calendar a, footer .ms-calendar tbody td, footer .ms-calendar tbody #today {
	color: #fff;
}

footer .ms-calendar tbody #today:hover, footer .ms-calendar tbody td:hover, footer .ms-calendar tfoot, footer .ms-calendar tfoot a {
	color: #333;
}
</style>

<?php } 

	function moneysite_custom_button_color() { 
	$moneysite_first_icon_bk_color = get_theme_mod('moneysite_first_icon_bk_color');
	$moneysite_first_icon_inner_color = get_theme_mod('moneysite_first_icon_inner_color');
	$moneysite_second_icon_bk_color = get_theme_mod('moneysite_second_icon_bk_color');
	$moneysite_second_icon_inner_color = get_theme_mod('moneysite_second_icon_inner_color');
	$moneysite_third_icon_bk_color = get_theme_mod('moneysite_third_icon_bk_color');
	$moneysite_third_icon_inner_color = get_theme_mod('moneysite_third_icon_inner_color');
	$moneysite_quote_button_bk_color = get_theme_mod('moneysite_quote_button_bk_color');
	$moneysite_quote_button_text_color = get_theme_mod('moneysite_quote_button_text_color');
	$moneysite_quote_button_hover_color = get_theme_mod('moneysite_quote_button_hover_color');
	$page_nav_bk_color = get_theme_mod('page_nav_bk_color');
	$page_nav_text_color = get_theme_mod('page_nav_text_color');
	$page_nav_hover_color = get_theme_mod('page_nav_hover_color');
	$page_nav_active_color = get_theme_mod('page_nav_active_color');
	$moneysite_first_icon_text_color = get_theme_mod('moneysite_first_icon_text_color');
	$moneysite_first_des_color = get_theme_mod('moneysite_first_des_color');
	$moneysite_second_icon_text_color = get_theme_mod('moneysite_second_icon_text_color');
	$moneysite_second_des_color = get_theme_mod('moneysite_second_des_color');
	$moneysite_third_icon_text_color = get_theme_mod('moneysite_third_icon_text_color');
	$moneysite_third_des_color = get_theme_mod('moneysite_third_des_color');
	$moneysite_button_bk_color = get_theme_mod('moneysite_button_bk_color');
	$moneysite_button_text_color = get_theme_mod('moneysite_button_text_color');
	$moneysite_button_hover_color = get_theme_mod('moneysite_button_hover_color');
	$grid_list_view_proprety = get_theme_mod('grid_list_view_proprety');

	$moneysite_fb_icon_bk_color = get_theme_mod('moneysite_fb_icon_bk_color');
	$moneysite_fb_icon_color = get_theme_mod('moneysite_fb_icon_color');
	$moneysite_fb_icon_hover_color = get_theme_mod('moneysite_fb_icon_hover_color');

	$moneysite_tw_icon_bk_color = get_theme_mod('moneysite_tw_icon_bk_color');
	$moneysite_tw_icon_color = get_theme_mod('moneysite_tw_icon_color');
	$moneysite_tw_icon_hover_color = get_theme_mod('moneysite_tw_icon_hover_color');

	$moneysite_link_icon_bk_color = get_theme_mod('moneysite_link_icon_bk_color');
	$moneysite_link_icon_color = get_theme_mod('moneysite_link_icon_color');
	$moneysite_link_icon_hover_color = get_theme_mod('moneysite_link_icon_hover_color');

	$moneysite_google_icon_bk_color = get_theme_mod('moneysite_google_icon_bk_color');
	$moneysite_google_icon_color = get_theme_mod('moneysite_goolge_icon_color');
	$moneysite_google_icon_hover_color = get_theme_mod('moneysite_google_icon_hover_color');		

?>

<style type="text/css">
	
	/* Start Grid/List View */

	<?php if($grid_list_view_proprety == 'list') : ?>
		.grid{
			grid-template-columns: 100%;
		}
	<?php endif;?>
	/* END Grid/List View */

	/* Start Header First Icon Colors */

	.first-icon .ms-header-box-info h4{
		color: <?php echo esc_attr( $moneysite_first_icon_text_color);?>;
	}

	.first-icon .ms-header-box-info p{
		color: <?php echo esc_attr( $moneysite_first_des_color);?>;
	}

	.ms-header-box.first-icon .ms-header-box-icon i{
		background:<?php echo esc_attr( $moneysite_first_icon_bk_color);?>;
	}

	.ms-header-box.first-icon .ms-header-box-icon i{
		color:<?php echo esc_attr( $moneysite_first_icon_inner_color);?>;
	}
	/* END Header First Icon Colors */

	/* Start Header Second Icon Colors */

	.second-icon .ms-header-box-info h4{
		color: <?php echo esc_attr( $moneysite_second_icon_text_color);?>;
	}

	.second-icon .ms-header-box-info p{
		color: <?php echo esc_attr( $moneysite_second_des_color);?>;
	}

	.ms-header-box.second-icon .ms-header-box-icon i{
		background:<?php echo esc_attr( $moneysite_second_icon_bk_color);?>;
	}

	.ms-header-box.second-icon .ms-header-box-icon i{
		color:<?php echo esc_attr( $moneysite_second_icon_inner_color);?>;
	}
	/* END Header Second Icon Colors */

	/* Start Header Third Icon Colors */

	.third-icon .ms-header-box-info h4{
		color: <?php echo esc_attr( $moneysite_third_icon_text_color);?>;
	}

	.third-icon .ms-header-box-info p{
		color: <?php echo esc_attr( $moneysite_third_des_color);?>;
	}

	.ms-header-box.third-icon .ms-header-box-icon i{
		background:<?php echo esc_attr( $moneysite_third_icon_bk_color);?>;
	}

	.ms-header-box.third-icon .ms-header-box-icon i{
		color:<?php echo esc_attr( $moneysite_third_icon_inner_color);?>;
	}
	/* END Header Third Icon Colors */

	/* Start Header Quote-button Icon Colors */
	.quote-button a{
		background:<?php echo esc_attr( $moneysite_quote_button_bk_color).' !important';?>;
	}

	.quote-button a{
		color:<?php echo esc_attr( $moneysite_quote_button_text_color).' !important';?>;
	}

	.quote-button a:hover{
		background:<?php echo esc_attr( $moneysite_quote_button_hover_color).' !important';?>;
	}
	/* END Header Quote-button Icon Colors */

	/* Start Page Navigation Colors */

	.nav-links a{
		background:<?php echo esc_attr( $page_nav_bk_color);?>;
	}

	.nav-links a{
		color:<?php echo esc_attr( $page_nav_text_color);?>;
	}

	.nav-links a:hover, .nav-links .page-numbers:hover{
		background-color:<?php echo esc_attr( $page_nav_hover_color);?>;
		background:<?php echo esc_attr( $page_nav_hover_color);?>;
	}

	.nav-links .page-numbers.current, .nav-links .page-numbers.current:hover{
		background:<?php echo esc_attr( $page_nav_active_color);?>;
	}
	
	/* END Page Navigation Colors */

	/* Start Social Icons Colors */

		/* FB Icon Colors*/
		.ms-social li span.fb a{
			background:<?php echo esc_attr( $moneysite_fb_icon_bk_color);?>;
		}

		.ms-social li span.fb a i{
			color:<?php echo esc_attr( $moneysite_fb_icon_color);?>;
		}

		.ms-social li span.fb a:hover{
			background:<?php echo esc_attr( $moneysite_fb_icon_hover_color);?>;
		}
		/* END FB Icon Colors*/

		/* Twitter Icon Colors*/
		.ms-social li span.twitter a{
			background:<?php echo esc_attr( $moneysite_tw_icon_bk_color);?>;
		}

		.ms-social li span.twitter a i{
			color:<?php echo esc_attr( $moneysite_tw_icon_color);?>;
		}

		.ms-social li span.twitter a:hover{
			background:<?php echo esc_attr( $moneysite_tw_icon_hover_color);?>;
		}
		/* END Twitter Icon Colors*/

		/* Linkedin Icon Colors*/
		.ms-social li span.linkedin a{
			background:<?php echo esc_attr( $moneysite_link_icon_bk_color);?>;
		}

		.ms-social li span.linkedin a i{
			color:<?php echo esc_attr( $moneysite_link_icon_color);?>;
		}

		.ms-social li span.linkedin a:hover{
			background:<?php echo esc_attr( $moneysite_link_icon_hover_color);?>;
		}
		/* END Linkedin Icon Colors*/

		/* Goolge-Plus Icon Colors*/
		.ms-social li span.google a{
			background:<?php echo esc_attr( $moneysite_google_icon_bk_color);?>;
		}

		.ms-social li span.google a i{
			color:<?php echo esc_attr( $moneysite_google_icon_color);?>;
		}

		.ms-social li span.google a:hover{
			background:<?php echo esc_attr( $moneysite_google_icon_hover_color);?>;
		}
		/* END Goolge-Plus Icon Colors*/				

	/* END Social Icons Colors */

	/* Start General Colors */

	.btn, footer .ms-widget.widget_search .btn, .ti_scroll, input[type="reset"], input[type="submit"]{
		background:<?php echo esc_attr( $moneysite_button_bk_color);?>;
		background-color:<?php echo esc_attr( $moneysite_button_bk_color);?>;
	}

	.btn, footer .ms-widget.widget_search .btn, .ti_scroll, input[type="reset"], input[type="submit"]{
		color:<?php echo esc_attr( $moneysite_button_text_color).' !important';?>;
		color:<?php echo esc_attr( $moneysite_button_text_color).' !important';?>;
	}

	.btn:hover, footer .ms-widget.widget_search .btn:hover, .ti_scroll:hover, input[type="reset"]:hover, input[type="submit"]:hover{
		background:<?php echo esc_attr( $moneysite_button_hover_color).' !important';?>;
		background-color:<?php echo esc_attr( $moneysite_button_hover_color).' !important';?>;
	}
	/* END General Colors */
</style>

<?php 
	}
?>