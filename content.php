<?php 
/**
 * The template for displaying the content.
 * @package moneysite
 */
?>

<div class="col-md-12">
	<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
		<div class="ms-blog-post-box">

			<?php
			$post_thumbnail_url = get_the_post_thumbnail( get_the_ID(), 'img-responsive' );
			if ( !empty( $post_thumbnail_url ) ) {
			?>
			<div class="meta-info-container">
				<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>" class="ms-blog-thumb">
							<?php echo wp_kses_post($post_thumbnail_url); ?>
				</a>
				<div class="clearfix"></div>

				<!-- Date Meta Data -->
				<span class="ms-blog-date"> 
					<span class="h3"><?php echo get_the_date('j'); ?></span> 
		  			<span><?php echo get_the_date('M').', '.get_the_date('Y'); ?></span>
			  	</span>
			  	<!-- Autor Meta Data -->
				<a href="<?php echo esc_url(get_author_posts_url( get_the_author_meta( 'ID' ) ));?>">
					<span class="ms-blog-author img-circle"> <?php echo get_avatar( get_the_author_meta( 'ID') , 64); ?> </span>
				</a>
			</div>
			<?php }	?>
			<article class="small">
				<h2><a title="<?php the_title_attribute(); ?>" href="<?php the_permalink(); ?>">
				  <?php the_title(); ?>
				  </a>
				</h2>
				<div class="ms-blog-category">
					<?php if(!has_post_thumbnail()) : ?>
						<a href="<?php echo get_day_link( get_the_date('Y'),get_the_date('m'),get_the_date('d')); ?>">
							<i class="fa fa-calendar"></i>
							<span><?php echo get_the_date('j').' '.get_the_date('M').', '.get_the_date('Y'); ?></span>
						</a>

						<a href="<?php echo esc_url(get_author_posts_url( get_the_author_meta( 'ID' ) ));?>"><i class="fa fa-user"></i>
						<?php the_author(); ?>
						</a>
					<?php endif; ?>
					
					<i class="fa fa-folder"></i>
					  <?php   $cat_list = get_the_category_list();
					  if(!empty($cat_list)) { ?>
					  <?php the_category(', '); ?>
					<?php } ?>
				</div>
				<?php
				$moneysite_more = strpos( $post->post_content, '<!--more' );
				if ( $moneysite_more ) :
					echo get_the_content();
				else :
					echo get_the_excerpt();
				endif;
				?>
				<?php wp_link_pages( array( 'before' => '<div class="link">' . __( 'Pages:', 'moneysite' ), 'after' => '</div>' ) ); ?>
			</article>
		</div>
	</div>
</div>