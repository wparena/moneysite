<?php
// Footer copyright section 
function moneysite_footer_copyright( $wp_customize ) {
	$wp_customize->add_panel('moneysite_copyright', array(
		'priority' => 51,
		'capability' => 'edit_theme_options',
		'title' => __('Theme Footer Settings', 'moneysite'),
	) );
	
	//Footer social link 
	$wp_customize->add_section('copyright_social_icon', array(
        'title' => __('Social Links Settings','moneysite'),
		'panel' => 'moneysite_copyright',
    ) );

	// Facebook link
	$wp_customize->add_setting('social_link_facebook', array(
        'sanitize_callback' => 'esc_url_raw',
    ) );
	$wp_customize->add_control('social_link_facebook', array(
        'label' => __('Facebook URL','moneysite'),
        'section' => 'copyright_social_icon',
        'type' => 'text',
    ) );

	$wp_customize->add_setting(
        'Social_link_facebook_tab',array(
        'sanitize_callback' => 'moneysite_copyright_sanitize_checkbox',
	));
	$wp_customize->add_control('Social_link_facebook_tab', array(
        'type' => 'checkbox',
        'label' => __('Open Link New tab/window','moneysite'),
        'section' => 'copyright_social_icon',
    ) );

    //Change icon backgorund color
    $wp_customize->add_setting('moneysite_fb_icon_bk_color', array(
    'default' => '#6A9FDF',
    'sanitize_callback' => 'sanitize_hex_color',
    ) );
    
    $wp_customize->add_control(new WP_Customize_Color_Control($wp_customize,'moneysite_fb_icon_bk_color', array(
        'label'      => __('fb icon background Color', 'moneysite' ),
        'section'    => 'copyright_social_icon',
        'settings'   => 'moneysite_fb_icon_bk_color',) 
    ) );

    //Change icon color
    $wp_customize->add_setting('moneysite_fb_icon_color', array(
    'default' => '#FFF',
    'sanitize_callback' => 'sanitize_hex_color',
    ) );
    
    $wp_customize->add_control(new WP_Customize_Color_Control($wp_customize,'moneysite_fb_icon_color', array(
        'label'      => __('fb icon color', 'moneysite' ),
        'section'    => 'copyright_social_icon',
        'settings'   => 'moneysite_fb_icon_color',) 
    ) );

    //Change icon hover color
    $wp_customize->add_setting('moneysite_fb_icon_hover_color', array(
    'default' => '#777',
    'sanitize_callback' => 'sanitize_hex_color',
    ) );
    
    $wp_customize->add_control(new WP_Customize_Color_Control($wp_customize,'moneysite_fb_icon_hover_color', array(
        'label'      => __('fb icon hover color', 'moneysite' ),
        'section'    => 'copyright_social_icon',
        'settings'   => 'moneysite_fb_icon_hover_color',) 
    ) );

	//Twitter link
	$wp_customize->add_setting( 'social_link_twitter', array(
       'sanitize_callback' => 'esc_url_raw',
    ) );
	$wp_customize->add_control( 'social_link_twitter', array(
        'label' => __('Twitter URL','moneysite'),
        'section' => 'copyright_social_icon',
        'type' => 'text',
    ) );

	$wp_customize->add_setting( 'Social_link_twitter_tab',array(
	   'sanitize_callback' => 'moneysite_copyright_sanitize_checkbox',
	) );

	$wp_customize->add_control( 'Social_link_twitter_tab', array(
        'type' => 'checkbox',
        'label' => __('Open Link New tab/window','moneysite'),
        'section' => 'copyright_social_icon',
    ) );

    //Change icon backgorund color
    $wp_customize->add_setting('moneysite_tw_icon_bk_color', array(
    'default' => '#6A9FDF',
    'sanitize_callback' => 'sanitize_hex_color',
    ) );
    
    $wp_customize->add_control(new WP_Customize_Color_Control($wp_customize,'moneysite_tw_icon_bk_color', array(
        'label'      => __('Twitter icon background Color', 'moneysite' ),
        'section'    => 'copyright_social_icon',
        'settings'   => 'moneysite_tw_icon_bk_color',) 
    ) );

    //Change icon color
    $wp_customize->add_setting('moneysite_tw_icon_color', array(
    'default' => '#FFF',
    'sanitize_callback' => 'sanitize_hex_color',
    ) );
    
    $wp_customize->add_control(new WP_Customize_Color_Control($wp_customize,'moneysite_tw_icon_color', array(
        'label'      => __('Twitter icon color', 'moneysite' ),
        'section'    => 'copyright_social_icon',
        'settings'   => 'moneysite_tw_icon_color',) 
    ) );

    //Change icon hover color
    $wp_customize->add_setting('moneysite_tw_icon_hover_color', array(
    'default' => '#777',
    'sanitize_callback' => 'sanitize_hex_color',
    ) );
    
    $wp_customize->add_control(new WP_Customize_Color_Control($wp_customize,'moneysite_tw_icon_hover_color', array(
        'label'      => __('Twitter icon hover color', 'moneysite' ),
        'section'    => 'copyright_social_icon',
        'settings'   => 'moneysite_tw_icon_hover_color',) 
    ) );

	//Linkdin link
	$wp_customize->add_setting( 'social_link_linkedin', array(
       'sanitize_callback' => 'esc_url_raw',
    ) );
	$wp_customize->add_control( 'social_link_linkedin', array(
        'label' => __('Linkedin URL','moneysite'),
        'section' => 'copyright_social_icon',
        'type' => 'text',
    ) );

	$wp_customize->add_setting( 
        'Social_link_linkedin_tab',array(
        'sanitize_callback' => 'moneysite_copyright_sanitize_checkbox',
	) );

	$wp_customize->add_control( 'Social_link_linkedin_tab', array(
        'type' => 'checkbox',
        'label' => __('Open Link New tab/window','moneysite'),
        'section' => 'copyright_social_icon',
    ) );


    //Change icon backgorund color
    $wp_customize->add_setting('moneysite_link_icon_bk_color', array(
    'default' => '#6A9FDF',
    'sanitize_callback' => 'sanitize_hex_color',
    ) );
    
    $wp_customize->add_control(new WP_Customize_Color_Control($wp_customize,'moneysite_link_icon_bk_color', array(
        'label'      => __('Linkedin icon background Color', 'moneysite' ),
        'section'    => 'copyright_social_icon',
        'settings'   => 'moneysite_link_icon_bk_color',) 
    ) );

    //Change icon color
    $wp_customize->add_setting('moneysite_link_icon_color', array(
    'default' => '#FFF',
    'sanitize_callback' => 'sanitize_hex_color',
    ) );
    
    $wp_customize->add_control(new WP_Customize_Color_Control($wp_customize,'moneysite_link_icon_color', array(
        'label'      => __('Linkedin icon color', 'moneysite' ),
        'section'    => 'copyright_social_icon',
        'settings'   => 'moneysite_link_icon_color',) 
    ) );

    //Change icon hover color
    $wp_customize->add_setting('moneysite_link_icon_hover_color', array(
    'default' => '#777',
    'sanitize_callback' => 'sanitize_hex_color',
    ) );
    
    $wp_customize->add_control(new WP_Customize_Color_Control($wp_customize,'moneysite_link_icon_hover_color', array(
        'label'      => __('Linkedin icon hover color', 'moneysite' ),
        'section'    => 'copyright_social_icon',
        'settings'   => 'moneysite_link_icon_hover_color',) 
    ) );

	//Google-plus link
	$wp_customize->add_setting('social_link_google', array(
        'sanitize_callback' => 'esc_url_raw',
    ) );
	$wp_customize->add_control('social_link_google', array(
        'label' => __('Google-plus URL','moneysite'),
        'section' => 'copyright_social_icon',
        'type' => 'text',
    ) );

	$wp_customize->add_setting(
        'Social_link_google_tab',array(
        'sanitize_callback' => 'moneysite_copyright_sanitize_checkbox',
	) );

	$wp_customize->add_control('Social_link_google_tab', array(
        'type' => 'checkbox',
        'label' => __('Open Link New tab/window','moneysite'),
        'section' => 'copyright_social_icon',
    ) );

    //Change icon backgorund color
    $wp_customize->add_setting('moneysite_google_icon_bk_color', array(
    'default' => '#6A9FDF',
    'sanitize_callback' => 'sanitize_hex_color',
    ) );
    
    $wp_customize->add_control(new WP_Customize_Color_Control($wp_customize,'moneysite_google_icon_bk_color', array(
        'label'      => __('Google-plus icon background Color', 'moneysite' ),
        'section'    => 'copyright_social_icon',
        'settings'   => 'moneysite_google_icon_bk_color',) 
    ) );

    //Change icon color
    $wp_customize->add_setting('moneysite_google_icon_color', array(
    'default' => '#FFF',
    'sanitize_callback' => 'sanitize_hex_color',
    ) );
    
    $wp_customize->add_control(new WP_Customize_Color_Control($wp_customize,'moneysite_google_icon_color', array(
        'label'      => __('Google-plus icon color', 'moneysite' ),
        'section'    => 'copyright_social_icon',
        'settings'   => 'moneysite_google_icon_color',) 
    ) );

    //Change icon hover color
    $wp_customize->add_setting('moneysite_google_icon_hover_color', array(
    'default' => '#777',
    'sanitize_callback' => 'sanitize_hex_color',
    ) );
    
    $wp_customize->add_control(new WP_Customize_Color_Control($wp_customize,'moneysite_google_icon_hover_color', array(
        'label'      => __('Google-plus icon hover color', 'moneysite' ),
        'section'    => 'copyright_social_icon',
        'settings'   => 'moneysite_google_icon_hover_color',) 
    ) );
		
	function moneysite_footer_copyright_sanitize_text( $input ) {

    return wp_kses_post( force_balance_tags( $input ) );

	}
	
	function moneysite_copyright_sanitize_checkbox( $checked ) {
	// Boolean check.
	return ( ( isset( $checked ) && true == $checked ) ? true : false );
	}
	
}
add_action( 'customize_register', 'moneysite_footer_copyright' );
?>