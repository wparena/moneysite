=== Moneysite ===

Contributors: moneysite
Requires at least: WordPress 4.7
Tested up to: WordPress 5.0-trunk
Version: 1.0.9
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html
Tags: one-column, two-columns, three-columns, right-sidebar, flexible-header, custom-background, custom-colors, custom-header, custom-menu, custom-logo, featured-images, footer-widgets, theme-options, threaded-comments, translation-ready, blog, grid-layout

== Description ==

MoneySite is a financial WordPress blog theme that is suitable for both fininial products and money products. It is highly responsive(desktops, tablets and smartphones).Moneysite is created using using Twitter Bootstrap Framework and works with both list view and grid view. The theme is highly customizable and one can change its entire look and colors by using the theme's customizer. You can also have a look at moneysite demo here http://moneysite.com/demo/ and can find more inforamtion about theme here https://www.moneysite.com/wordpress-theme/ . MoneySite is a great design idea for websites related to finance, cryptocurrency, trading, money-transfers and many more.
Create Outstanding Website or Blog in Minutes!.

Theme has two column layout. The content on left side and a right sidebar widget. We focused on usability across various devices, starting with smart phones.it is compatible with various devices. MoneySite is a Cross-Browser Compatible theme that works on All leading web browsers. MoneySite is easy to use and user friendly theme.

Powerful but simple MoneySite theme content customized through customizer. to make your site attractive it has two widget sections first for sidebar widget section and second for Footer widget sections.

To make your website in two column use sidebar widget section. to set custom menu in header set primary location. we added social media links to added your social links.It boasts of beautifully designed page sections , Home, Blog and Default Page Template(page with right sidebar). MoneySite theme has more advanced feature to make your site awesome like: it has header top bar dark & lite color feature. you can also changed header color dark and lite styling. MoneySite is translation ready theme.

This theme is compatible with Wordpress Version 4.7  and above and it supports the new theme customization API (https://codex.wordpress.org/Theme_Customization_API).

Supported browsers: Firefox, Opera, Chrome, Safari and IE11 (Some css3 styles like shadows, rounder corners and 2D transform are not supported by IE8 and below).

== Installation ==

1. In your admin panel, go to Appearance -> Themes and click the 'Add New' button.
2. Type in Moneysite in the search form and press the 'Enter' key on your keyboard.
3. Click on the 'Activate' button to use your new theme right away.
5. Navigate to Appearance > Customize in your admin panel and customize to taste.

== Copyright ==

MoneySite WordPress Theme, Copyright (C) 2018 Jazib Zaman
MoneySite is distributed under the terms of the GNU GPL
This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 2 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

MoneySite bundles the following third-party resources:

==========================================
This theme uses Bootstrap as a design tool
==========================================
* Bootstrap v3.3.7 (http://getbootstrap.com)
* Copyright 2011-2016 Twitter, Inc.
* Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)

================================================
This theme uses Font Awesome for the theme icons
================================================
* Font Awesome 4.7.0 by @davegandy - http://fontawesome.io - @fontawesome
* License - http://fontawesome.io/license (Font: SIL OFL 1.1, CSS: MIT License)

========================================================
/*! SmartMenus jQuery Plugin - v1.0.0 - January 27, 2016
========================================================
 * http://www.smartmenus.org/
 * Copyright Vasil Dinkov, Vadikom Web Ltd. http://vadikom.com; Licensed MIT */

===============================
This theme uses daneden/animate
===============================
* Animate  : https://github.com/daneden
* Copyright (c) 2018 Daniel Eden ( https://github.com/daneden/animate.css/blob/master/LICENSE )
* Licensed under https://github.com/daneden/animate.css

== Changelog ==

= 1.0.9 =

* Released: April 19, 2018
* Set The Footer Section

= 1.0.8 =

* Released: April 18, 2018
* Added Options In Customizer

= 1.0.7 =

* Released: April 13, 2018
* Small Error Fixes

= 1.0.6 =

* Released: April 12, 2018
* Small Error Fixes

= 1.0.5 =

* Released: Mar 29, 2018
* Added Option In Customizer for Icon and Button colors

= 1.0.4 =

* Released: Mar 26, 2018
* Dequeue Extra Files

= 1.0.3 =

* Released: Mar 22, 2018
* Applied Proper Escaping For Dynamic Data

= 1.0.2 =

* Released: Mar 21, 2018
* Error/Bugs fixes

= 1.0.1 =

* Released: Feb 3, 2018
* Addition Of Some Css Rules
* Some Modification In Readme File

= 1.0 =

* Released: January 27, 2018
Initial release