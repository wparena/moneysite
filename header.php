<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @package moneysite
 */

?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div class="wrapper">
<header>
  <div class="clearfix"></div>
  <div class="ms-main-nav">
    <div class="container">
      <div class="row">
        <div class="col-md-3">
          <div class="navbar-header">
          <!-- Logo -->
          <?php
          if(has_custom_logo()){
            // Display the Custom Logo
            the_custom_logo();
          }
          else { ?>
            <a class="navbar-brand" href="<?php echo esc_url(home_url( '/' )); ?>"><?php bloginfo('name'); ?>
  		      <br>
            <span class="site-description"><?php echo esc_html(get_bloginfo( 'description')); ?></span>   
            </a>      
          <?php } ?>
          <!-- Logo -->
          </div>
        </div>
        <div class="col-md-9 hidden-xs hidden-sm">
          <div class="header-widget">
            <div class="col-md-3 col-sm-3">
              <div class="ms-header-box first-icon animated flipInX">
                <div class="ms-header-box-icon">
                  <?php $moneysite_header_widget_one_icon = get_theme_mod('moneysite_header_widget_one_icon');
                  if( !empty($moneysite_header_widget_one_icon) ):
                    echo '<i class="'.esc_attr($moneysite_header_widget_one_icon).'">'.'</i>';
                  endif; ?>
                </div>
                <div class="ms-header-box-info">
                  <?php $moneysite_header_widget_one_title = get_theme_mod('moneysite_header_widget_one_title'); 
                  if( !empty($moneysite_header_widget_one_title) ):
                    echo '<h4>'.esc_attr($moneysite_header_widget_one_title).'</h4>';
                  endif; ?>
                  <?php $moneysite_header_widget_one_description = get_theme_mod('moneysite_header_widget_one_description');
                  if( !empty($moneysite_header_widget_one_description) ):
                    echo '<p>'.esc_attr($moneysite_header_widget_one_description).'</p>';
                  endif; ?> 
                </div>
              </div>
            </div>
            <div class="col-md-3 col-sm-3">
              <div class="ms-header-box second-icon animated flipInX">
                <div class="ms-header-box-icon">
                  <?php $moneysite_header_widget_two_icon = get_theme_mod('moneysite_header_widget_two_icon');
                  if( !empty($moneysite_header_widget_two_icon) ):
                    echo '<i class="'.esc_attr($moneysite_header_widget_two_icon).'">'.'</i>';
                  endif; ?>
                 </div>
                <div class="ms-header-box-info">
                  <?php $moneysite_header_widget_two_title = get_theme_mod('moneysite_header_widget_two_title'); 
                  if( !empty($moneysite_header_widget_two_title) ):
                    echo '<h4>'.esc_attr($moneysite_header_widget_two_title).'</h4>';
                  endif; ?>
                  <?php $moneysite_header_widget_two_description = get_theme_mod('moneysite_header_widget_two_description');
                  if( !empty($moneysite_header_widget_two_description) ):
                    echo '<p>'.esc_attr($moneysite_header_widget_two_description).'</p>';
                  endif; ?> 
                </div>
              </div>
            </div>
            <div class="col-md-3 col-sm-3">
              <div class="ms-header-box wow third-icon animated flipInX">
                <div class="ms-header-box-icon">
                  <?php $moneysite_header_widget_three_icon = get_theme_mod('moneysite_header_widget_three_icon');
                  if( !empty($moneysite_header_widget_three_icon) ):
                    echo '<i class="'.esc_attr($moneysite_header_widget_three_icon).'">'.'</i>';
                  endif; ?>
                 </div>
                <div class="ms-header-box-info">
                  <?php $moneysite_header_widget_three_title = get_theme_mod('moneysite_header_widget_three_title'); 
                  if( !empty($moneysite_header_widget_three_title) ):
                    echo '<h4>'.esc_attr($moneysite_header_widget_three_title).'</h4>';
                  endif; ?>
                  <?php $moneysite_header_widget_three_description = get_theme_mod('moneysite_header_widget_three_description');
                  if( !empty($moneysite_header_widget_three_description) ):
                    echo '<p>'.esc_attr($moneysite_header_widget_three_description).'</p>';
                  endif; ?> 
                </div>
              </div>
            </div>
            <div class="col-md-3 col-sm-3">
              <div class="ms-header-box wow quote-button animated flipInX text-right"> 
                <?php $moneysite_header_widget_four_label = esc_attr(get_theme_mod('moneysite_header_widget_four_label')); 
                $moneysite_header_widget_four_link = esc_url(get_theme_mod('moneysite_header_widget_four_link'));
                $moneysite_header_widget_four_target = esc_attr(get_theme_mod('moneysite_header_widget_four_target')); 

                if( !empty($moneysite_header_widget_four_label) ):?>
                  <a href="<?php echo esc_url($moneysite_header_widget_four_link); ?>" <?php if( $moneysite_header_widget_four_target ==true) { echo "target='_blank'"; } ?> class="btn btn-quote">
                    <?php echo esc_attr($moneysite_header_widget_four_label); ?>
                  </a> 
                <?php endif; ?>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <nav class="navbar navbar-default navbar-static-top navbar-wp">
      <div class="container"> 
        <!-- navbar-toggle -->
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-wp"> <span class="sr-only"><?php __('Toggle Navigation','moneysite'); ?></span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
        <!-- /navbar-toggle --> 
        <!-- Navigation -->
        
        <div class="collapse navbar-collapse" id="navbar-wp">
          <?php wp_nav_menu( array( 'theme_location' => 'primary', 'container' => false, 'menu_class' => 'nav navbar-nav', 'fallback_cb' => 'moneysite_custom_navwalker::fallback' , 'walker' => new moneysite_custom_navwalker() ) ); ?>
        </div>
        <!-- /Navigation --> 
      </div>
    </nav>
  </div>

</header>
<!-- #masthead --> 