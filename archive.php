<?php
/**
 * The template for displaying archive pages.
 *
 * @package moneysite
 */

get_header(); ?>
<main id="content">
  <div class="container">
    <div class="row">
      	<div class="<?php echo ( !is_active_sidebar( 'sidebar-1' ) ? 'col-md-12 col-sm-12' :'col-md-9 col-sm-8' ); ?>">
	        <div class="row">
				<h1 class="archive-page-heading"><?php echo wp_kses_post(get_the_archive_title()); ?></h1>
	        	<div class="grid">
					<?php 
					if( have_posts() ) :
					while( have_posts() ): the_post();
					get_template_part('content',''); 
					endwhile; endif;
					?>
		          </div>
	        </div>
	        <div class="col-md-12 text-center">
	          	<?php
					//Previous / next page navigation
					the_posts_pagination( array(
					'prev_text'          => '<i class="fa fa-long-arrow-left"></i>',
					'next_text'          => '<i class="fa fa-long-arrow-right"></i>',
					'screen_reader_text' => ' ',
					) );
					?>
	        </div>
      	</div>
	  	<aside class="col-md-3 col-sm-4">
        	<?php get_sidebar(); ?>
      	</aside>
    </div>
  </div>
</main>
<?php get_footer(); ?>